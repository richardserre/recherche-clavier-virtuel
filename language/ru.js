jQuery.keyboard.language.ru={
	language:"Русский (Russian)",
	display:{
		a:"✔:Сохранить (Shift+Enter)",
		accept:"Сохранить:Сохранить (Shift+Enter)",
		alt:"РУС:Русская клавиатура",
		b:"←:Удалить символ слева",
		bksp:"⇦:Удалить символ слева",
		c:"✖:Отменить (Esc)",
		cancel:"Отменить:Отменить (Esc)",
		clear:"C:Очистить",
		combo:"ö:Toggle Combo Keys",
		dec:",:Decimal",
		e:"↵:Ввод",
		enter:"Ввод:Перевод строки",
		lock:"⇪ Lock:Caps Lock",
		s:"⇧:Верхний регистр",
		shift:"⇧:Верхний регистр",
		sign:"±:Сменить знак",
		space:"Пробел:",
		t:"⇥:Tab",
		tab:"⇥ Tab:Tab"
	},
	wheelMessage:"Use mousewheel to see other keys"
		
};

