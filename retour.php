<?php
	if (!isset($_GET['lang'])) {
		echo "<br/> pbs ajax <br/>";
	} else {
		$ok=true;
		$lang=$_GET['lang'];
		if ($lang=='arabic') {
			echo "<script src='layouts/ms-Arabic.js'></script>";
			echo "<script src='language/ar.js'></script>";
			$layout="{layout: 'ms-Arabic (102) AZERTY' } ";
		} elseif ($lang=='russian') {
			echo "<script src='layouts/russian.js'></script>";
			echo "<script src='language/ru.js'></script>";
			$layout="{layout: 'russian-mac' } ";
		} elseif ($lang=='chinese') {
			echo "<script src='layouts/chinese.js'></script>";
			echo "<script src='language/zh.js'></script>";
			$layout="{layout: 'chinese' } ";
		} elseif ($lang=='dvorak') {
			$layout="{layout: 'dvorak' }";	
		} elseif ($lang=='hebreu') {
			echo "<script src='layouts/ms-Hebrew.js'></script>";
			echo "<script src='language/he.js'></script>";
			$layout="{layout: 'ms-Hebrew' }";	
		} elseif ($lang=='japonais') {
			echo "<script src='layouts/ms-Japanese-Hiragana.js'></script>";
			echo "<script src='language/ja.js'></script>";
			$layout="{layout: 'ms-Japanese Hiragana' }";	
		} elseif ($lang=='coreen') {
			echo "<script src='layouts/ms-Korean.js'></script>";
			$layout="{layout: 'ms-Korean' }";	
		}elseif ($lang=='perse') {
			echo "<script src='layouts/ms-Persian.js'></script>";
			echo "<script src='language/fa.js'></script>";
			$layout="{layout: 'ms-Persian' }";	
		} elseif ($lang=='rien') {
			$ok=false;
		}
		if ($ok) {
			echo "<script>
				var vkb=$('#inputprincipal').getkeyboard();
				if (vkb) { 
					vkb.destroy(); 
				}
				var vkb2=$('#inputprincipal').keyboard(".$layout.");
				var vkb3=$('#inputprincipal').getkeyboard();
				vkb3.reveal();
			</script>";
		} else {
			echo "<script>
				var vkb=$('#inputprincipal').getkeyboard();
				if (vkb) { 
					vkb.destroy(); 
				}
			
			</script>";		
			
			
			
		}
	}	
		



?>
